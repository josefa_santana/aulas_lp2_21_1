<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contas extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('ContasModel', 'conta');
    }
    public function index(){
        echo "Lista de todas as contas";
    }

    public function pagar($mes = 0, $ano = 0){
        $this->defineMonth($mes, $ano);

        // salva os dados da nova conta caso exista
        $this->conta->cria('pagar');

        // recupera a lista de contas a pagar
        $v['lista'] = $this->conta->lista('pagar', $mes, $ano);
        $v['tipo'] = 'pagar';

        // carrega a view e passa a lista de contas
        $html = $this->load->view('contas/lista_contas', $v,true);

        // exibe esta view
        $this->show($html);
    }

    public function receber($mes = 0, $ano = 0){
        $this->defineMonth($mes, $ano);
        
        // salva os dados da nova conta caso exista
        $this->conta->cria('receber');

        // recupera a lista de contas a pagar
        $v['lista'] = $this->conta->lista('receber', $mes, $ano);
        $v['tipo'] = 'receber';

        // carrega a view e passa a lista de contas
        $html = $this->load->view('contas/lista_contas', $v,true);

        // exibe esta view
        $this->show($html);
    }

    public function movimento($mes = 0, $ano = 0){
        $this->defineMonth($mes, $ano);

        $v['lista'] = $this->conta->lista('mista', $mes, $ano);
        $html = $this->load->view('contas/lista_contas', $v,true);
        $this->show($html);
    }

    private function defineMonth(&$mes, &$ano){
        $mes = $mes ? $mes : date('m');
        $ano = $ano ? $ano : date('Y');
        $_POST['month'] = "$ano-$mes";
    }
}